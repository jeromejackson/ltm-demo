#ifndef __atm_bloechl_hh__
#define __atm_bloechl_hh__

// Blöchl, Jepsen & Andersen's (PRB 49, 1994, 16223--16233) formulation of d(e) and n(e)
// and Lambin & Vigneron's (PRB 29, 1984, 3430--3437) symmetric formulation

#include <cstddef> // for size_t

// delta function (contribution of one tetrahedron to n(eref)), symmetric version
// despite appealing form, this is more prone to rounding than the Blöchl code
/*
template <typename T>
T dfun(T e1, T e2, T e3, T e4, T eref) {
        // sort eigenvalues at four vertices
        T t;
        if (e1 > e2) {
                t = e2;
                e2 = e1;
                e1 = t;
        }
        if (e3 > e4) {
                t = e4;
                e4 = e3;
                e3 = t;
        }
        if (e1 > e3) {
                t = e3;
                e3 = e1;
                e1 = t;
        }
        if (e2 > e4) {
                t = e4;
                e4 = e2;
                e2 = t;
        }
        if (e2 > e3) {
                t = e3;
                e3 = e2;
                e2 = t;
        }

        // special case when all eval equal and equal to reference case
        if ( eref == e1 && e1 == e2 && e2 == e3 && e3 == e4 ) {
                return 1;
        }

        if ( e1 < eref && eref <= e2) {
                return 3 * (eref - e1) * (eref - e1) / ((e2 - e1) * (e3 - e1) * (e4 - e1));
        } else if (e2 < eref && eref < e3) {
                return 3 * ( (e3 - eref) * (eref - e2) / ((e4 - e2) * (e3 - e2) * (e3 - e1)) +
                             (e4 - eref) * (eref - e1) / ((e4 - e1) * (e4 - e2) * (e3 - e1)) );
        } else if (e3 <= eref && eref < e4) {
                return 3 * (e4 - eref) * (e4 - eref) / ((e4 - e1) * (e4 - e2) * (e4 - e3));
        } else {
                return 0;
        }
}
*/

// assymetric version from Blöchl
template <typename T>
T dfun(T e1, T e2, T e3, T e4, T eref) {

        // sort eigenvalues at four vertices
        T t;
        if (e1 > e2) {
                t = e2;
                e2 = e1;
                e1 = t;
        }
        if (e3 > e4) {
                t = e4;
                e4 = e3;
                e3 = t;
        }
        if (e1 > e3) {
                t = e3;
                e3 = e1;
                e1 = t;
        }
        if (e2 > e4) {
                t = e4;
                e4 = e2;
                e2 = t;
        }
        if (e2 > e3) {
                t = e3;
                e3 = e2;
                e2 = t;
        }

        if (eref < e1 || eref > e4) {
                return 0.0;
        } else if (eref > e1 && eref <= e2) {
                return 3 * (eref - e1) * (eref - e1) / ((e2 - e1) * (e3 - e1) * (e4 - e1));
        } else if (eref > e2 && eref <= e3) {
                return (3 * (e2 - e1) + 6 * (eref - e2) - 3 * ((e3 - e1) + (e4 - e2)) * (eref - e2) * (eref - e2) / ((e3 - e2) * (e4 - e2)))
                        / ((e3 - e1) * (e4 - e1));
        } else if (eref > e3 && eref <= e4) {
                return 3 * (e4 - eref) * (e4 - eref) / ((e4 - e1) * (e4 - e2) * (e4 - e3));
        } else {
                return 1.0; // eref = e1 = e2 = e3 = e4
        }
}

// step function (contribution of one tetrahedron to integrated n(neref))
// assymetric version from Blöchl
template <typename T>
T sfun(T e1, T e2, T e3, T e4, T eref) {
        // sort eigenvalues at four vertices
        T t;
        if (e1 > e2) {
                t = e2;
                e2 = e1;
                e1 = t;
        }
        if (e3 > e4) {
                t = e4;
                e4 = e3;
                e3 = t;
        }
        if (e1 > e3) {
                t = e3;
                e3 = e1;
                e1 = t;
        }
        if (e2 > e4) {
                t = e4;
                e4 = e2;
                e2 = t;
        }
        if (e2 > e3) {
                t = e3;
                e3 = e2;
                e2 = t;
        }

        if (eref < e1) {
                return 0;
        } else if (eref > e1 && eref <= e2) {
                return (eref - e1) * (eref - e1) * (eref - e1) / ((e2 - e1) * (e3 - e1) * (e4 - e1));
        } else if (eref > e2 && eref <= e3) {
                return ((e2 - e1) * (e2 - e1) + 3 * (e2 - e1) * (eref - e2) + 3 * (eref - e2) * (eref - e2)
                               - ((e3 - e1) + (e4 - e2)) * (eref - e2) * (eref - e2) * (eref - e2) / ((e3 - e2) * (e4 - e2)))
                        / ((e3 - e1) * (e4 - e1));
        } else if (eref > e3 && eref <= e4) {
                return 1 - (e4 - eref) * (e4 - eref) * (e4 - eref) / ((e4 - e1) * (e4 - e2) * (e4 - e3));
        } else {
                return 1; // eref > e4 (or all equal)
        }
}

// step function (contribution of one tetrahedron to integrated F(k,e))
// assymetric version from Blöchl
template <typename T>
void tetw(T e1, T e2, T e3, T e4, T eref, T *wgts) {
        // sort eigenvalues at four vertices (recording order)
        size_t order[4] { 0, 1, 2, 3 };
        size_t ti;
        T t;
        if (e1 > e2) {
                t = e2;
                e2 = e1;
                e1 = t;
                ti = order[1];
                order[1] = order[0];
                order[0] = ti;
        }
        if (e3 > e4) {
                t = e4;
                e4 = e3;
                e3 = t;
                ti = order[3];
                order[3] = order[2];
                order[2] = ti;
        }
        if (e1 > e3) {
                t = e3;
                e3 = e1;
                e1 = t;
                ti = order[2];
                order[2] = order[0];
                order[0] = ti;
        }
        if (e2 > e4) {
                t = e4;
                e4 = e2;
                e2 = t;
                ti = order[3];
                order[3] = order[1];
                order[1] = ti;
        }
        if (e2 > e3) {
                t = e3;
                e3 = e2;
                e2 = t;
                ti = order[2];
                order[2] = order[1];
                order[1] = ti;
        }

        // weights for each kpoint
        T w[4];

        if (eref < e1) {
                w[0] = T(0);
                w[1] = T(0);
                w[2] = T(0);
                w[3] = T(0);
        } else if (eref > e1 && eref <= e2) {
                T c = (eref - e1) * (eref - e1) * (eref - e1) / (T(4) * (e2 - e1) * (e3 - e1) * (e4 - e1));
                w[0] = c * (T(4) - (eref - e1) * (T(1) / (e2 - e1) + T(1) / (e3 - e1) + T(1) / (e4 - e1)));
                w[1] = c * (eref - e1) / (e2 - e1);
                w[2] = c * (eref - e1) / (e3 - e1);
                w[3] = c * (eref - e1) / (e4 - e1);
        } else if (eref > e2 && eref <= e3) {
                T c1 = (eref - e1) * (eref - e1) / (T(4) * (e4 - e1) * (e3 - e1));
                T c2 = (eref - e1) * (eref - e2) * (e3 - eref) / (T(4) * (e4 - e1) * (e3 - e2) * (e3 - e1));
                T c3 = (eref - e2) * (eref - e2) * (e4 - eref) / (T(4) * (e4 - e2) * (e3 - e2) * (e4 - e1));
                w[0] = c1 + (c1 + c2) * (e3 - eref) / (e3 - e1) + (c1 + c2 + c3) * (e4 - eref) / (e4 - e1);
                w[1] = c1 + c2 + c3 + (c2 + c3) * (e3 - eref) / (e3 - e2) + c3 * (e4 - eref) / (e4 - e2);
                w[2] = (c1 + c2) * (eref - e1) / (e3 - e1) + (c2 + c3) * (eref - e2) / (e3 - e2);
                w[3] = (c1 + c2 + c3) * (eref - e1) / (e4 - e1) + c3 * (eref - e2) / (e4 - e2);
        } else if (eref > e3 && eref <= e4) {
                T c = (e4 - eref) * (e4 - eref) * (e4 - eref) / (T(4) * (e4 - e1) * (e4 - e2) * (e4 - e3));
                w[0] = T(1) / T(4) - c * (e4 - eref) / (e4 - e1);
                w[1] = T(1) / T(4) - c * (e4 - eref) / (e4 - e2);
                w[2] = T(1) / T(4) - c * (e4 - eref) / (e4 - e3);
                w[3] = T(1) / T(4) - c * (T(4) - (T(1) / (e4 - e1) + T(1) / (e4 - e2) + T(1) / (e4 - e3)) * (e4 - eref));
        } else {
                w[0] = T(1) / T(4);
                w[1] = T(1) / T(4);
                w[2] = T(1) / T(4);
                w[3] = T(1) / T(4);
        }

        // Blöchl correction term
        /*
        T def = dfun(e1,e2,e3,e4,eref); // contribution n(E_F) from this tet
        if(def<1e3 && def>0){
        w[0] += (def / 40.0) * ((e4 - e1) + (e3 - e1) + (e2 - e1));
        w[1] += (def / 40.0) * ((e4 - e2) + (e3 - e2) + (e1 - e2));
        w[2] += (def / 40.0) * ((e4 - e3) + (e2 - e3) + (e1 - e3));
        w[3] += (def / 40.0) * ((e3 - e4) + (e2 - e4) + (e1 - e4));
        }
        */

        for (size_t i = 0; i < 4; ++i) wgts[order[i]] = w[i];
}
#endif
