#ifndef __tet_bz_hh__
#define __tet_bz_hh__

#include "atm-bloechl.hh"
#include <algorithm>
#include <vector>

// data for one k-point
template <typename T>
struct kpt {
        T x, y, z, wgt;

        kpt(T x, T y, T z, T w)
                : x { x }
                , y { y }
                , z { z }
                , wgt { w } { };
};

// tetrahedron definition
struct tet {
        size_t multi;
        size_t ik[4];

        tet(size_t k1, size_t k2, size_t k3, size_t k4) {
                multi = 1;

                // sort to enable comparison later
                size_t t;
                if (k1 > k2) {
                        t = k2;
                        k2 = k1;
                        k1 = t;
                }
                if (k3 > k4) {
                        t = k4;
                        k4 = k3;
                        k3 = t;
                }
                if (k1 > k3) {
                        t = k3;
                        k3 = k1;
                        k1 = t;
                }
                if (k2 > k4) {
                        t = k4;
                        k4 = k2;
                        k2 = t;
                }
                if (k2 > k3) {
                        t = k3;
                        k3 = k2;
                        k2 = t;
                }

                ik[0] = k1;
                ik[1] = k2;
                ik[2] = k3;
                ik[3] = k4;
        };
        tet( ) { }; // for omp
};

// c++20 gives a default: https://en.cppreference.com/w/cpp/language/default_comparisons
inline bool operator==(const tet &a, const tet &b) {
        return a.ik[0] == b.ik[0] && a.ik[1] == b.ik[1] && a.ik[2] == b.ik[2] && a.ik[3] == b.ik[3];
}

inline bool operator!=(const tet &a, const tet &b) {
        return !(a == b);
}

inline bool operator<(const tet &a, const tet &b) {
        if (b.ik[0] < a.ik[0]) {
                return true;
        } else if (b.ik[0] > a.ik[0]) {
                return false;
        } else if (b.ik[1] < a.ik[1]) {
                return true;
        } else if (b.ik[1] > a.ik[1]) {
                return false;
        } else if (b.ik[2] < a.ik[2]) {
                return true;
        } else if (b.ik[2] > a.ik[2]) {
                return false;
        } else if (b.ik[3] < a.ik[3]) {
                return true;
        } else if (b.ik[3] < a.ik[3]) {
                return false;
        } else {
                // they are equal
                return false;
        }
}

template <typename T>
struct bz {
        size_t nkfbz, numtetfbz; // total num tets, size of eval/evec arrays
        std::vector<kpt<T>> kpts; // list of k-point vectors
        std::vector<size_t> map2ibz; // map2ibz[i] = j in IBZ (numbering in reduced k list)
        std::vector<tet> tets; // list of tetrahedra

        bz<T>(int[3], int *, int (*)[3], T[3][3]);
        T dos(T, size_t, T **) const; // n(E)
        T intdos(T, size_t, T **) const; // integrated n(E)
        void weights(T, size_t, T **, T **) const; // integrated n(E) weights
};

template <typename T>
bz<T>::bz(int nk[3], int *map, int (*grid)[3], T rlat[3][3]) {

        auto ijk2ind = [&](size_t i, size_t j, size_t k) {
                size_t ii = (i + nk[0]) % nk[0];
                size_t jj = (j + nk[1]) % nk[1];
                size_t kk = (k + nk[2]) % nk[2];
                // return ii + jj * nk[0] + kk * nk[0] * nk[1];
                size_t ikfbz = ii + jj * nk[0] + kk * nk[0] * nk[1]; // in fbz numbering
                return map2ibz[ikfbz]; // in ibz numbering
        };

        nkfbz = nk[0] * nk[1] * nk[2]; // note type conversion
        map2ibz.resize(nkfbz);
        std::vector<size_t> mlt(nkfbz); // multiplicity (becomes floating weight)

        // setup symmetry reduced list of k-points (ordered)
        size_t count = 0;
        for (size_t i = 0; i < nkfbz; ++i) {
                mlt[i] = 0;
                if (size_t(map[i]) == i) map2ibz[i] = count++;
        }
        for (size_t i = 0; i < nkfbz; ++i) {
                mlt[map[i]]++;
                map2ibz[i] = map2ibz[map[i]];
        }
        // this should be parallelised, but order of kpts list must be preserved
        for (size_t i = 0; i < nkfbz; ++i) {
                if (mlt[i] > 0) {
                        size_t gi = ((grid[i][0] + nk[0]) % nk[0]); // integer k point on [0,nk[0])
                        size_t gj = ((grid[i][1] + nk[1]) % nk[1]);
                        size_t gk = ((grid[i][2] + nk[2]) % nk[2]);

                        T weight = T(mlt[i]) / T(nkfbz); // sum of weights over BZ = 1

                        T fa = T(gi) / T(nk[0]);
                        T fb = T(gj) / T(nk[1]);
                        T fc = T(gk) / T(nk[2]);

                        T kx = fa * rlat[0][0] + fb * rlat[1][0] + fc * rlat[2][0];
                        T ky = fa * rlat[0][1] + fb * rlat[1][1] + fc * rlat[2][1];
                        T kz = fa * rlat[0][2] + fb * rlat[1][2] + fc * rlat[2][2];

                        kpts.push_back({ kx, ky, kz, weight });
                }
        }

        // setup full list of tetrahedra
        tets.resize(6 * nkfbz); // full (later to be reduced) list of tetrahedra
#pragma omp parallel for
        for (size_t i = 0; i < nkfbz; ++i) {

                size_t gi = grid[i][0];
                size_t gj = grid[i][1];
                size_t gk = grid[i][2];

                // coordinates of vertices: 1  0, 2  +x, 3  +y, 4  +x+y, 5  +z, 6  +x+z, 7  +y+z, 8  +x+y+z
                size_t k1 = ijk2ind(gi, gj, gk);
                size_t k2 = ijk2ind(gi + 1, gj, gk);
                size_t k3 = ijk2ind(gi, gj + 1, gk);
                size_t k4 = ijk2ind(gi + 1, gj + 1, gk);
                size_t k5 = ijk2ind(gi, gj, gk + 1);
                size_t k6 = ijk2ind(gi + 1, gj, gk + 1);
                size_t k7 = ijk2ind(gi, gj + 1, gk + 1);
                size_t k8 = ijk2ind(gi + 1, gj + 1, gk + 1);

                // one particular choice of tetrahedra: 1356 1236 2346 3468 3678 3567
                tets[6 * i] = tet(k1, k3, k5, k6);
                tets[6 * i + 1] = tet(k1, k2, k3, k6);
                tets[6 * i + 2] = tet(k2, k3, k4, k6);
                tets[6 * i + 3] = tet(k3, k4, k6, k8);
                tets[6 * i + 4] = tet(k3, k6, k7, k8);
                tets[6 * i + 5] = tet(k3, k5, k6, k7);
        }
        // sort to facilitate removing duplicates
        // fixme: this is usually the most expensive part of this setup
        // for parallel algorithm see: https://gcc.gnu.org/onlinedocs/libstdc++/manual/parallel_mode_using.html
        sort(tets.begin( ), tets.end( ));

        // remove duplicates
        // fixme, parallelise and reduce partial lists
        std::vector<tet> tets_red;
        tets_red.push_back(tets[0]);
        for (size_t it = 1; it < tets.size(); ++it) {
                if (tets[it] != tets_red.back()) {
                        tets_red.push_back(tets[it]);
                } else {
                        tets_red.back().multi++;
                }
        }
        tets = tets_red;

        // count total tets
        numtetfbz = 0;
        for (auto &t : tets) numtetfbz += t.multi;
}

// the following two could be simplified with function pointers?, but it's very slow :/
// density of states, d(E)
template <typename T>
T bz<T>::dos(T z, size_t ndham, T **e) const {
        T integ { 0.0 };
#pragma omp parallel for reduction(+ : integ)
        for (size_t it = 0; it < tets.size( ); ++it) {
                tet t = tets[it];
                size_t ik1 = t.ik[0];
                size_t ik2 = t.ik[1];
                size_t ik3 = t.ik[2];
                size_t ik4 = t.ik[3];
                for (size_t i = 0; i < ndham; ++i) {
                        integ += t.multi * dfun(e[ik1][i], e[ik2][i], e[ik3][i], e[ik4][i], z);
                }
        }
        return integ / numtetfbz;
}

// integrated density of states, n(E)
template <typename T>
T bz<T>::intdos(T z, size_t ndham, T **e) const {
        T integ { 0.0 };
#pragma omp parallel for reduction(+ : integ)
        for (size_t it = 0; it < tets.size( ); ++it) {
                tet t = tets[it];
                size_t ik1 = t.ik[0];
                size_t ik2 = t.ik[1];
                size_t ik3 = t.ik[2];
                size_t ik4 = t.ik[3];
                for (size_t i = 0; i < ndham; ++i) {
                        integ += t.multi * sfun(e[ik1][i], e[ik2][i], e[ik3][i], e[ik4][i], z);
                }
        }
        return integ / numtetfbz;
}

// k-point weights
template <typename T>
void bz<T>::weights(T z, size_t ndham, T **e, T **w) const {
        // zero arrays
        for (size_t i = 0; i < kpts.size( ); ++i) {
                for (size_t j = 0; j < ndham; ++j) {
                        w[i][j] = 0;
                }
        }

        // fixme parallelise please (reducing w)
        for (size_t it = 0; it < tets.size( ); ++it) {
                tet t = tets[it];
                size_t ik1 = t.ik[0];
                size_t ik2 = t.ik[1];
                const size_t ik3 = t.ik[2];
                size_t ik4 = t.ik[3];
                T wtmp[4];
                for (size_t i = 0; i < ndham; ++i) {
                        tetw(e[ik1][i], e[ik2][i], e[ik3][i], e[ik4][i], z, wtmp);
                        w[ik1][i] += wtmp[0] * T(t.multi) / T(numtetfbz);
                        w[ik2][i] += wtmp[1] * T(t.multi) / T(numtetfbz);
                        w[ik3][i] += wtmp[2] * T(t.multi) / T(numtetfbz);
                        w[ik4][i] += wtmp[3] * T(t.multi) / T(numtetfbz);
                }
        }
}
#endif
