#include "tet-bz.hh"
#include <cassert>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <spglib.h>
#include <cmath> // for M_PI

using namespace std;

using real_t = double;

int main(int argc, char *argv[]) {

        /*
         * usage ./a.out ref-data nk
         * ref-data is file containing two columns, real energies and n(e)
         * nk is a (hopefully positive!) integer specifying the kmesh eg 10 for 10x10x10
         *
         * sorry, this file is really junk
         *
         */


        // read reference data from file specified as arg(1)
        char *fn = argv[1];
        ifstream reffile(fn, ios::in);
        vector<real_t> elist;
        vector<real_t> refdat;
        real_t z, d;
        reffile >> z >> d;
        while (!reffile.eof( )) {
                elist.push_back(z);
                refdat.push_back(d);
                reffile >> z >> d;
        }

        // junk for crystal structure
        // for simple cubic, reciprocal lattice matches RS lattice (but for 2pi)
        double pdble[3][3] { { 1.0, 0.0, 0.0 }, { 0.0, 1.0, 0.0 }, { 0.0, 0.0, 1.0 } };
        real_t p[3][3] { { 1.0, 0.0, 0.0 }, { 0.0, 1.0, 0.0 }, { 0.0, 0.0, 1.0 } };
        double(*pos)[3] = new double[1][3];
        pos[0][0] = 0.0;
        pos[0][1] = 0.0;
        pos[0][2] = 0.0;
        int *types = new int[1];
        types[0] = 0;

        // read in nk from arg(2)
        int arg = atoi(argv[2]);
        int nkabc[3] { arg, arg, arg };
        int nkfbz = nkabc[0] * nkabc[1] * nkabc[2];
        int is_shift[3] { 0, 0, 0 };

        // spglib outputs
        int *map = new int[nkfbz]; // map[i] = j in IBZ (numbering corresponding to original list)
        int(*grid)[3] = new int[nkfbz][3]; // ijk coordinates of each k

        // spglib only supports double precision!
        int ngen = spg_get_ir_reciprocal_mesh(grid, map, nkabc, is_shift, true, pdble, pos, types, 1, 1e-9);

        // setup bz
        //
        //
        //
        const bz<real_t> bzdata(nkabc, map, grid, p);
        //
        //
        //
        //

        // check all kpts found
        assert(bzdata.kpts.size( ) == size_t(ngen));
        assert(bzdata.numtetfbz == size_t(nkfbz * 6));

        // setup eigenvalues on kpts in ibz
        int ndham { 1 }; // only one band here
        real_t **eig = new real_t *[ngen]; // 2d data array (pointers to 1d arrays)
        for (int i = 0; i < ngen; ++i) {
                real_t kx = bzdata.kpts[i].x;
                real_t ky = bzdata.kpts[i].y;
                real_t kz = bzdata.kpts[i].z;

                eig[i] = new real_t[ndham];
                eig[i][0] = cos(kx * 2 * M_PI) + cos(ky * 2 * M_PI) + cos(kz * 2 * M_PI);
        }

        // evaluate error at list of real energies
        real_t rmserr { 0.0 };
        real_t maxerr { 0.0 };
        for (size_t i = 0; i < elist.size( ); ++i) {
                real_t z = elist[i];

                // obtain n(E) 
                //
                //
                real_t res = M_PI * bzdata.dos(z, ndham, eig);
                //
                //
                //
                real_t err = res - refdat[i];
                rmserr += err * err;
                if (abs(err) > maxerr) maxerr = abs(err);
                cerr << setw(20) << z << setw(20) << res << setw(20) << sqrt(err * err) << endl;
        }
        rmserr = sqrt(rmserr / elist.size( ));
        // cube root (number of kpts FBZ), number of k-points IBZ, RMS dos error
        cout << setw(10) << arg << setw(10) << ngen << setw(20) << rmserr << setw(20) << maxerr << endl;

        return 0;
}
