
## toy linear tetrahedron method demonstration for simple cubic tight binding model

|                           |                                       |
|---------------------------|---------------------------------------|
| 3d-tb.dat                 | reference data, see below             |
| atm-bloechl.hh            | basic expressions of linear method     |
| float.cc                  | toy driver for simple cubic TB case   |
| ltm-sc-tb-mode-error.dat  | output of the code up to meshes 800^3 |
| makefile                  | GNU make file                         |
| tet-bz.hh                 | some basic classes, see below         |


### 3d-tb.dat

Reference density of states for the 3D simple cubic tight-binding problem calculated using the semi-analytic expression:

$$
    n(E) = \int_0^\infty cos(x*E) J_0^3(x) dx
$$

where $`J_0`$ is the Bessel function of the first kind of order 0.  This (improper) integral converges terribly for the van Hove singularities ($`E=1`$ and $`E=3`$).

### tet-bz.hh

Basic machinery for canonical linear tetrahedron method.

Class "bz" is initialised with: 

    const bz<real_t> bzdata(nkabc, map, grid, p);

Where `nkabc` is of type `int[3]`, `map` and `grid` are the outputs of spglib ( https://spglib.readthedocs.io/en/latest/ ) and `p` is the reciprocal lattice `double[3][3]`.

The generated kpoints are accessible via the member `kpts` which is a vector of simple objects which contain the Cartesian $`k_x`$, etc.

A member function:

    double res = M_PI * bzdata.dos(E, ndham, eig);

returns the density of states at $`E`$ (real only for now), using eigenvalues listed in the `double**` array `eig[nk_ibz][ndham]`, which must be dimensioned by the number of k-points in the IBZ (returned by `spglib` and also the result of `bzdata.kpts.size()`) and `ndham` is the number of bands (here there is only one).

- although templated, on my machine (x86_64) float and double perform pretty identically; only float uses less memory
- the code will spit out the calculated density of states to standard error and print the RMS error to standard output
- the code is pretty basic and doesn't check anything for error, etc, and accepts horrible c-like arguments: sorry about this

### notes

- the RMS error scales as $`1/(N_k)^{1/3}`$, i.e. inverse to the cube root of the total number of k-points (so, 1 over the number of divisions in each direction)

![linear tet error](ltm-sc-tb-model-error.svg)

- the tetrahedron method performs terribly at the van Hove singularities, in particular at $`E=1`$, and this dominates the error

![linear tet error distribution](ltm-sc-tb-model-error-dist.svg)


