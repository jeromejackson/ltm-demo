# assumes CXX is gcc, for clang++ link to -lomp instead of -lgomp, etc
CXXFLAGS=-Wall -Ofast -fopenmp --std=c++11 -D_GLIBCXX_PARALLEL 
LDFLAGS=-lsymspg -lgomp

sp.x: float.o
	$(CXX) $(LDFLAGS) $^ -o $@

float.o: atm-bloechl.hh tet-bz.hh makefile

clean:
	@rm -f sp.x float.o
